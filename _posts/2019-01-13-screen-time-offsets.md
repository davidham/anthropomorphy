---
layout: post
title:  "Screen time offsets"
date:   2019-01-13 16:04:00 -0500
categories:
---

Like many people this time of year, I am looking for ways to be more mindful about my device usage. I found this delightful article ["In Search of Lost Screen Time"](https://www.nytimes.com/2018/12/31/opinion/smartphones-screen-time.html).

> In 2018 those 253 million Americans spent $1,380 and 1,460 hours on their smartphone and other mobile devices.

It goes on to describe some of the things you could do with all that time and money.

> In most Western states, that $1,380 you spent on your phone could buy half an acre of land. In the right conditions, that half acre could easily accommodate 150 trees. A single tree sequesters 48 pounds of carbon a year. It takes about 30 minutes for an amateur forester to plant a tree. If every American smartphone owner used that time and money to plant half an acre of trees, we would sequester about 886 million tons of carbon a year, enough to offset more than 10 percent of the country’s annual emissions. If you don't want to do the planting yourself, the National Forest Foundation says it could meet all of its planting goals if every smartphone user gave it just 60 cents.

And:

> Every year 10 million tons of plastic waste flows into the ocean. According to George Leonard of the Ocean Conservancy, if Americans applied all the money they allocate to smartphones to solving plastic pollution, “There would be enough money available to pay for the necessary improvements in waste management in Asian countries for 70 years.” And if the time Americans spent on smartphones were applied to ocean clean up at a rate of five pounds of plastic garbage per person per hour, “The volunteer effort could clean up the amount of plastic that flows into the global ocean 118 times over.”
